﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Model
{
    [Serializable]
    public class Code
    {
        public Code()
        {
        }

        public int codeID { get; set; }
        public String code { get; set; }
        public int videoId { get; set; }
        public bool isRedeemed { get; set; }
        public int? redeemedNo { get; set; }
        public DateTime? expiryDate { get; set; }
        public bool? isAdmin { get; set; }

        public bool ValidateForInsert()
        {
            throw new NotImplementedException();
        }

        public bool ValidateForUpdate()
        {
            throw new NotImplementedException();
        }

    }
}
