﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Model
{
    [Serializable]
    public class Video
    {
        public Video()
        {
        }

        public int videoId { get; set; }
        public string videoName { get; set; }
        public string schoolName { get; set; }
        public string videoFilePath { get; set; }

        public bool ValidateForInsert()
        {
            throw new NotImplementedException();
        }

        public bool ValidateForUpdate()
        {
            throw new NotImplementedException();
        }

    }
}
