﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Model
{
    [Serializable]
    public class Redemption
    {
        public Redemption()
        {
        }

        public int redemptionId { get; set; }
        public string code { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string IP { get; set; }
        public string Browser { get; set; }
        public bool isSucess { get; set; }
        public DateTime dateTimeStamp { get; set; }

        public bool ValidateForInsert()
        {
            throw new NotImplementedException();
        }

        public bool ValidateForUpdate()
        {
            throw new NotImplementedException();
        }

    }
}
