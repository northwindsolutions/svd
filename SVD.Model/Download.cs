﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Model
{
    [Serializable]
    public class Download
    {
        public Download()
        {
        }

        public int downloadId { get; set; }
        public int redemptionId { get; set; }
        public string code { get; set; }
        public bool? isSuccess { get; set; }
        public DateTime? dateTimeStamp { get; set; }

        public bool ValidateForInsert()
        {
            throw new NotImplementedException();
        }

        public bool ValidateForUpdate()
        {
            throw new NotImplementedException();
        }

    }
}
