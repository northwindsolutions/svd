﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SVD._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="text-center">
        <img src="https://www.schoolvideo.co.uk/images/logosml.png" />
             </div>
        <h1 class="svHeader">School Video Downloader</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula eget arcu eget ultrices. Aenean consequat venenatis justo a rutrum. Ut in elementum ipsum. Sed sed lorem non mi varius molestie. </p>
        <p class="lead">Nullam iaculis augue fringilla tortor aliquam venenatis. Curabitur pulvinar consectetur nulla ac semper. Proin placerat ipsum sed sapien faucibus, a iaculis tortor tincidunt. Donec sit amet lobortis risus.</p>
       
       <div class="card p-2">
        <div class="input-group">
          <asp:TextBox CssClass="form-control" ID="tbAcessCode" runat="server" placeholder="Access Code" ></asp:TextBox>
          <div class="input-group-append">
              <asp:Button ID="btnRedeem" runat="server" CssClass="btn btn-primary" Text="Redeem" OnClick="btnRedeem_Click" />
          </div>

            <asp:Label ID="lblAccessCodeError" Visible="false" runat="server" CssClass="alert-warning" Text="Code Not Valid, Please Try Again"></asp:Label>

        </div>
      </div>
    </div>




  
    

</asp:Content>
