﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace SVD.AccessCode
{
    public partial class Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string accessCode = checkAccessCode();
            if (string.IsNullOrEmpty(accessCode))
            {
                errorOrNotLoggedIn();
            }
            else
            {
                Model.Code c = new Service.svcCode().GetCode(accessCode);
                if (c == null)
                {
                    errorOrNotLoggedIn();
                }
                else
                {
                    Model.Video video = new Service.svcVideo().GetVideo(c.videoId);
                    lblVideoName.Text = video.videoName;
                    lblSchoolName.Text = video.schoolName;
                }
            }
        }

        private string checkAccessCode()
        {
            string accessCode = (string)GetSession(Consts.ssnSVDAccessCode);
            if (string.IsNullOrEmpty(accessCode))
            {
                errorOrNotLoggedIn();
            }
            return accessCode;
        }

        private void errorOrNotLoggedIn()
        {
            Response.Redirect(Consts.RouteFriendlyHome, true);
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Model.Code c = new Service.svcCode().GetCode(checkAccessCode());
            if (c == null)
            {
                errorOrNotLoggedIn();
            }
            else
            {
                Model.Video video = new Service.svcVideo().GetVideo(c.videoId);
                if (video != null)
                {
                    string fileName = video.videoFilePath;
                    if (!string.IsNullOrEmpty(video.videoFilePath))
                    {
                        RemoveSession(Consts.ssnSVDAccessCode);

                        Model.Download d = new Model.Download();
                        d.code = c.code;
                        d.dateTimeStamp = DateTime.Now;
                        d.redemptionId = 1; //todo
                        d = new Service.svcDownloads().InsertDownload(d);

                        if (d != null && d.downloadId > 0)
                        {
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlPathEncode(video.videoName));
                            Response.ContentType = "video/mp4";
                            Response.AddHeader("Content-Length", new FileInfo(fileName).Length.ToString());
                            Response.TransmitFile(fileName);
                            Response.Flush();

                            try
                            {
                                Response.End();
                            }
                            finally
                            {
                                d.isSuccess = true;
                                new Service.svcDownloads().UpdateDownload(d);
                            }
                        }
                    }
                }
            }
        }

        protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
        {
            checkAccessCode();
        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Wizard1.ActiveStepIndex == 1)
            {
                if (Page.IsValid)
                {
                    Model.Redemption r = new Model.Redemption();
                    r.firstName = tbFirstName.Text;
                    r.lastName = tbLastName.Text;
                    r.email = tbEmail.Text;
                    r.IP = Request.UserHostAddress;
                    r.Browser = Request.Browser.Browser;
                    r.code = checkAccessCode();
                    r.dateTimeStamp = DateTime.Now;
                    r.isSucess = true;
                    r = new Service.svcRedemptions().InsertRedemption(r);
                    if (r == null)
                    {
                        errorOrNotLoggedIn();
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        protected void rfv_cbTerms_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (cbTerms.Checked)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

    }
}