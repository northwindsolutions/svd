﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SVD.AccessCode.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="jumbotron">
    <div class="text-center">
        <div class="form-signin">
            <img src="https://www.schoolvideo.co.uk/images/logosml.png" />
<asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" DisplayCancelButton="false" DisplaySideBar="false" AllowReturn="false" 
    OnActiveStepChanged="Wizard1_ActiveStepChanged" OnNextButtonClick="Wizard1_NextButtonClick"  >
    <WizardSteps>
        <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1" StepType="Start" AllowReturn="false" >

            You Are Downloading:
            <h2 class="svHeader"><asp:Label ID="lblVideoName" runat="server" /></h2>

            - by - 

            <h2 class="svHeader"><asp:Label ID="lblSchoolName" runat="server" /></h2>

            <p style="font-size: 12px;">Not correct? <a href="https://www.schoolvideo.co.uk/contact.aspx">Contact Us.</a></p>

            
        </asp:WizardStep>
        
        <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2" StepType="Step" AllowReturn="false" >

            <h2 class="svHeader">We Just Need Some Details</h2>
            <div class="inputDetails">
                <asp:TextBox CssClass="form-control" ID="tbFirstName" runat="server" placeholder="First Name" ></asp:TextBox>
                <asp:RequiredFieldValidator id="rfv_tbFirstName" runat="server" ControlToValidate="tbFirstName" Display="Dynamic">Please enter your first name</asp:RequiredFieldValidator>

                <asp:TextBox CssClass="form-control" ID="tbLastName" runat="server" placeholder="Last Name" ></asp:TextBox>
                <asp:RequiredFieldValidator id="rfv_tbLastName" runat="server" ControlToValidate="tbLastName" Display="Dynamic">Please enter your last name</asp:RequiredFieldValidator>

                <asp:TextBox CssClass="form-control" ID="tbEmail" runat="server" placeholder="Email" TextMode="Email" ></asp:TextBox>
                <asp:RequiredFieldValidator id="rfv_tbEmail" runat="server" ControlToValidate="tbEmail" Display="Dynamic">Please enter your email</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rev_tbEmail" runat="server" ControlToValidate="tbEmail" Display="Dynamic" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" >Please enter a valid email</asp:RegularExpressionValidator>
            
                <p><asp:CheckBox ID="cbTerms" runat="server" CausesValidation="true" ></asp:CheckBox>
                I agree to the <a target="_blank" href="https://www.schoolvideo.co.uk/terms.aspx">Terms and Conditions</a></p>
                <asp:CustomValidator runat="server" ID="rfv_cbTerms" Display="Dynamic" OnServerValidate="rfv_cbTerms_ServerValidate">You must accept the terms and conditions to proceed.</asp:CustomValidator>
            </div>


            
        </asp:WizardStep>


        <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3" StepType="Complete" AllowReturn="false" >

            <h2 class="svHeader">Your Download is Ready</h2>

        <p>Please somehting and something</p>

                <asp:button id="btnDownload" runat="server" class="btn btn-primary" Text="Download" OnClick="btnDownload_Click"></asp:button>
            <br /><br /><br /><br />
            <p>Download complete? Check out our <a href="http://www.schoolvideo.co.uk">website.</a></p>
        </asp:WizardStep>

        
    </WizardSteps>
</asp:Wizard>
        </div>
        </div>
        </div>

    </asp:Content>
