﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SVD.Service;

namespace SVD
{
    public partial class _Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRedeem_Click(object sender, EventArgs e)
        {
            //check valid
            string code = tbAcessCode.Text.Trim();
            Model.Code validCode = new Service.svcCode().GetValidCode(code, Request.UserHostAddress);
            if(validCode != null && validCode.codeID > 0)
            {
                SetSession(Consts.ssnSVDAccessCode, code);
                if(validCode.isAdmin == true)
                {
                    Response.Redirect(Consts.RouteFriendlyAdmin, true);
                }
                else if (new Service.svcCode().UpdateCodeRedeemed(validCode))
                {
                    Response.Redirect(Consts.RouteFriendlyAccessPath, true);
                }
            }
            lblAccessCodeError.Visible = true;
        }
    }
}
