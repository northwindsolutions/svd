﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SVD
{
    public static class Consts
    {
        //Session
        public const String ssnSVDAccessCode = "SVD.AccessCode";

        //routes
        public const String RouteFriendlyHome = "/";
        public const String RouteFriendlyAccessPath = "accesscode/";
        public const String RouteFriendlyAdmin = "admin/";
        public const String RouteFriendlyAccessPathComplete = "accesscode/complete.aspx";
    }
}