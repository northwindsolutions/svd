﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SVD.Model;
using SVD.Data;

namespace SVD
{
    public class BasePage : System.Web.UI.Page
    {
        protected IDataRep dataRep { get; set; }

        public void SetSession(String sessionName, Object value)
        {
            HttpContext.Current.Session.Remove(sessionName);
            HttpContext.Current.Session.Add(sessionName, value);
        }        
        public object GetSession(String sessionName)
        {
            return HttpContext.Current.Session[sessionName];
        }
        public void RemoveSession(String sessionName)
        {
            HttpContext.Current.Session.Remove(sessionName);
        }
    }
}