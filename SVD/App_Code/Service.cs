﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SVD.Model;
using SVD.Data;
using System.Linq.Expressions;
namespace SVD.Service
{
    public class svcCode : ServiceBase
    {
        public svcCode() 
        {

        }

        public Model.Code GetCode(string code)
        {
            return dataRep.GetCodes().Where(c => c.code == code).FirstOrDefault();
        }
        public Model.Code GetValidCode(string code, string userIp)
        {
            Model.Code validCode = dataRep.GetCodes().Where(c => c.code == code && (c.expiryDate == null || c.expiryDate > DateTime.Now) && (c.redeemedNo == null || c.redeemedNo < 3)).FirstOrDefault();
            if (validCode != null && validCode.isRedeemed)
            {
                Model.Redemption red = dataRep.GetRedemptions().Where(w => w.code == code).FirstOrDefault();
                if (red != null)
                {
                    if (red.IP != userIp)
                    {
                        validCode = null;
                    }
                }
            }
            return validCode;
        }
        public Model.Code InsertCode(Model.Code c)
        {
            return dataRep.InsertCode(c);
        }

        public Boolean UpdateCodeRedeemed(Model.Code c)
        {
            if (c.isAdmin == false)
            {
                c.isRedeemed = true;
                if (c.redeemedNo == null)
                {
                    c.redeemedNo = 1;
                }
                else
                {
                    c.redeemedNo = c.redeemedNo += 1;
                }
                
            }
            return dataRep.UpdateCode(c);
        }
    }

    public class svcVideo : ServiceBase
    {
        public svcVideo()
        {

        }

        public Model.Video GetVideo(int videoId)
        {
            List<Model.Video> vs = dataRep.GetVideos().ToList();
            return dataRep.GetVideos().Where(v => v.videoId == videoId).FirstOrDefault();
        }
        public List<Model.Video> GetVideos()
        {
            return dataRep.GetVideos().ToList();
        }
        public Model.Video InsertVideo(Model.Video v)
        {
            return dataRep.InsertVideo(v);
        }


    }

    public class svcRedemptions : ServiceBase
    {
        public svcRedemptions()
        {

        }

        public Model.Redemption InsertRedemption(Model.Redemption r)
        {
            return dataRep.InsertRedemption(r);
        }

    }

    public class svcDownloads : ServiceBase
    {
        public svcDownloads()
        {

        }

        public Model.Download InsertDownload(Model.Download d)
        {
            return dataRep.InsertDownload(d);
        }

        public Boolean UpdateDownload(Model.Download d)
        {
            return dataRep.UpdateDownload(d);
        }

    }
}