﻿using System;
using System.Configuration;
using SVD.Data;

namespace SVD.Service
{
    public class ServiceBase : IDisposable
    {
        protected IDataRep dataRep { get; set; }

        public ServiceBase()
        {
            //setup the dataRepository

            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"] !=
                null ? ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString : string.Empty;

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Missing connection string");
            }
            else
            {
                dataRep = new SQLDataRep(connectionString);
            }
        }

        public void Dispose()
        {
            if (dataRep != null)
            {
                dataRep.Dispose();
            }
        }
    }


}