﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SVD.Admin._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="text-center">
        <img src="https://www.schoolvideo.co.uk/images/logosml.png" />
             </div>        

        <h2 class="svHeader">Admin</h2>
            Add New Video and create access codes:
            <div class="inputDetails">
                <asp:TextBox CssClass="form-control" ID="tbProductionName" runat="server" placeholder="Production Name" ></asp:TextBox>               

                <asp:TextBox CssClass="form-control" ID="tbFilePath" runat="server" placeholder="Path To File" ></asp:TextBox>                

                <asp:TextBox CssClass="form-control" ID="tbSchoolName" runat="server" placeholder="School Name"  ></asp:TextBox>

                <asp:TextBox CssClass="form-control" ID="tbCodesRequired" runat="server" placeholder="Number Of Codes Required" TextMode="Number" ></asp:TextBox>

                <asp:TextBox CssClass="form-control" ID="tbDaysValid" runat="server" placeholder="Days Code Valid" TextMode="Number" ></asp:TextBox>
                
                <asp:button id="btnSave" runat="server" class="btn btn-primary" Text="Save and Generate" OnClick="btnSave_Click"></asp:button>

                <p><asp:Label ID="lblCodes" runat="server"></asp:Label></p>
                
            </div>


            Create new codes for Existing Video:
            <div class="inputDetails">
                <asp:DropDownList CssClass="form-control" ID="ddlProductions" DataTextField="videoName" DataValueField="videoId" runat="server" placeholder="Production Name" ></asp:DropDownList>               

                <asp:TextBox CssClass="form-control" ID="tbVideoCodesRequired" runat="server" placeholder="Number Of Codes Required" TextMode="Number" ></asp:TextBox>

                <asp:TextBox CssClass="form-control" ID="tbVideoDaysValid" runat="server" placeholder="Days Code Valid" TextMode="Number" ></asp:TextBox>
                
                <asp:button id="btnGenerate" runat="server" class="btn btn-primary" Text="Generate" OnClick="btnGenerate_Click"></asp:button>

                <p><asp:Label ID="lblVideoCodes" runat="server"></asp:Label></p>
                
            </div>
        
       
      
    </div>




  
    

</asp:Content>
