﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SVD.Service;

namespace SVD.Admin
{
    public partial class _Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlProductions.DataSource = new Service.svcVideo().GetVideos();
            ddlProductions.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Model.Video v = new Model.Video();
            v.videoName = tbProductionName.Text;
            v.videoFilePath = tbFilePath.Text;
            v.schoolName = tbSchoolName.Text;
            v = new svcVideo().InsertVideo(v);
            if(v != null && v.videoId > 0)
            {
                lblCodes.Text = "Generated Codes:";
                generateAndSaveCodes(v.videoId);

            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            lblVideoCodes.Text = "Generated Codes:";
            int videoId = Convert.ToInt32(ddlProductions.SelectedValue);
            generateAndSaveCodes(videoId, true);
        }

        private void generateAndSaveCodes(int videoId, bool existing = false)
        {
            int codesreq = existing ? Convert.ToInt32(tbVideoCodesRequired.Text) : Convert.ToInt32(tbCodesRequired.Text);
            List<string> codes = generateNumbers(codesreq);
            if (codes != null && codes.Count > 0)
            {
                foreach (string code in codes)
                {
                    Model.Code c = new Model.Code();
                    c.code = code;
                    if (existing && !string.IsNullOrEmpty(tbVideoDaysValid.Text))
                    {
                        c.expiryDate = DateTime.Now.AddDays(Convert.ToInt32(tbVideoDaysValid.Text));
                    }
                    else if (!existing && !string.IsNullOrEmpty(tbDaysValid.Text))
                    {
                        c.expiryDate = DateTime.Now.AddDays(Convert.ToInt32(tbDaysValid.Text));
                    }
                    c.isAdmin = false;
                    c.isRedeemed = false;
                    c.videoId = videoId;
                    c = new svcCode().InsertCode(c);

                    if (existing)
                    {
                        lblVideoCodes.Text += "<br /> " + code;
                    }
                    else if (!existing)
                    {
                        lblCodes.Text += "<br /> " + code;
                    }
                    
                }
            }
        }



        private List<string> generateNumbers(int amount)
        {
            List<string> randomList = new List<string>();
            for (int i = 0; randomList.Count < amount; i++)
            {
                string newString = RandomString(6);
                if (!randomList.Contains(newString))
                {
                    Model.Code c = new svcCode().GetCode(newString);
                    if (c == null)
                    {
                        randomList.Add(newString);
                    }
                }
            }
            return randomList;
        }

        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

       


    }
}
