﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Data
{
    public class SQLDataRep : IDataRep
    {
        private String _connectionString { get; set; }
        private SVDDataClassesDataContext dc;
        public SQLDataRep(string connectionString)
        {
            _connectionString = connectionString;
            dc = new SVDDataClassesDataContext(connectionString);
        }
        public void Dispose()
        {
            this.dc.Dispose();
        }
        

        public IQueryable<Model.Code> GetCodes()
        {
            return dc.Codes.Select(t1 => new Model.Code()
            {
                codeID = t1.codeId,
                code = t1.code1,
                expiryDate = t1.expiryDate,
                isAdmin = t1.isAdmin,
                isRedeemed = t1.isRedeemed,
                redeemedNo = t1.RedeemedNo,
                videoId = t1.videoId
            });
        }
        public Model.Code InsertCode(Model.Code c)
        {
            try
            {
                Code t1 = new Code();
                t1.code1 = c.code;
                t1.expiryDate = c.expiryDate;
                t1.isAdmin = c.isAdmin;
                t1.isRedeemed = c.isRedeemed;
                t1.RedeemedNo = c.redeemedNo;
                t1.videoId = c.videoId;
                dc.Codes.InsertOnSubmit(t1);
                dc.SubmitChanges();
                c.codeID = t1.codeId;
                return c;
            }
            catch
            {
                return null;
            }
        }
        public Boolean UpdateCode(Model.Code c)
        {
            try
            {
                Code t1 = dc.Codes.Where(t => t.codeId == c.codeID).FirstOrDefault();
                t1.code1 = c.code;
                t1.expiryDate = c.expiryDate;
                t1.isAdmin = c.isAdmin;
                t1.isRedeemed = c.isRedeemed;
                t1.RedeemedNo = c.redeemedNo;
                t1.videoId = c.videoId;
                dc.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }



        public IQueryable<Model.Video> GetVideos()
        {
            return dc.Videos.Select(t1 => new Model.Video()
            {
                videoId = t1.videoId,
                videoName = t1.videoName,
                schoolName = t1.schoolName,
                videoFilePath = t1.videoFilePath
            });
        }
        public Model.Video InsertVideo(Model.Video v)
        {
            try
            {
                Video t1 = new Video();
                t1.videoName = v.videoName;
                t1.schoolName = v.schoolName;
                t1.videoFilePath = v.videoFilePath;
                dc.Videos.InsertOnSubmit(t1);
                dc.SubmitChanges();
                v.videoId = t1.videoId;
                return v;
            }
            catch
            {
                return null;
            }
        }



        public IQueryable<Model.Redemption> GetRedemptions()
        {
            return dc.Redemptions.Select(t1 => new Model.Redemption()
            {
                redemptionId = t1.redemptionId,
                firstName = t1.firstName,
                lastName = t1.lastName,
                email = t1.email,
                IP = t1.IP,
                Browser = t1.Browser,
                code = t1.code,
                dateTimeStamp = t1.datetimeStamp,
                isSucess = t1.isSuccess
            });
        }
        public Model.Redemption InsertRedemption(Model.Redemption r)
        {
            try
            {
                Redemption t1 = new Redemption();
                t1.firstName = r.firstName;
                t1.lastName = r.lastName;
                t1.email = r.email;
                t1.IP = r.IP;
                t1.Browser = r.Browser;
                t1.code = r.code;
                t1.Browser = r.Browser;
                t1.datetimeStamp = r.dateTimeStamp;
                t1.isSuccess = r.isSucess;
                dc.Redemptions.InsertOnSubmit(t1);
                dc.SubmitChanges();
                r.redemptionId = t1.redemptionId;
                return r;
            }
            catch
            {
                return null;
            }
        }



        public IQueryable<Model.Download> GetDownloads()
        {
            return dc.Downloads.Select(t1 => new Model.Download()
            {
                downloadId = t1.downloadId,
                redemptionId = t1.redemptionId,
                code = t1.code,
                dateTimeStamp = t1.datetimeStamp,
                isSuccess = t1.isSuccess
            });
        }
        public Model.Download InsertDownload(Model.Download d)
        {
            try
            {
                Download t1 = new Download();
                t1.redemptionId = d.redemptionId;
                t1.code = d.code;
                t1.datetimeStamp = d.dateTimeStamp;
                t1.isSuccess = d.isSuccess;
                dc.Downloads.InsertOnSubmit(t1);
                dc.SubmitChanges();
                d.downloadId = t1.downloadId;
                return d;
            }
            catch
            {
                return null;
            }
        }
        public Boolean UpdateDownload(Model.Download d)
        {
            try
            {
                Download t1 = dc.Downloads.Where(t2 => t2.downloadId == d.downloadId).FirstOrDefault();
                t1.redemptionId = d.redemptionId;
                t1.code = d.code;
                t1.datetimeStamp = d.dateTimeStamp;
                t1.isSuccess = d.isSuccess;
                dc.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

       
    }
}
