﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVD.Data
{
    public interface IDataRep : IDisposable
    {
        IQueryable<Model.Code> GetCodes();
        Model.Code InsertCode(Model.Code c);
        Boolean UpdateCode(Model.Code c);

        IQueryable<Model.Video> GetVideos();
        Model.Video InsertVideo(Model.Video v);

        IQueryable<Model.Redemption> GetRedemptions();
        Model.Redemption InsertRedemption(Model.Redemption r);

        IQueryable<Model.Download> GetDownloads();
        Model.Download InsertDownload(Model.Download d);
        Boolean UpdateDownload(Model.Download d);


    }
}
